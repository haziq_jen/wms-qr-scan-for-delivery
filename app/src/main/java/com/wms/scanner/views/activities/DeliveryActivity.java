package com.wms.scanner.views.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.wms.scanner.R;
import com.wms.scanner.controllers.ApplicationController;
import com.wms.scanner.controllers.HttpController;
import com.wms.scanner.cores.common.DatabaseHandler;
import com.wms.scanner.cores.common.SessionManager;
import com.wms.scanner.cores.common.utility.Utility;
import com.wms.scanner.models.data.GsonItems;
import com.wms.scanner.models.data.auth.UserItems;
import com.wms.scanner.models.data.dbdeliver.DeliverDetailDBModel;
import com.wms.scanner.models.data.deliver.DeliveryHeaderModel;
import com.wms.scanner.models.data.shipout.ShipOutDetailModel;
import com.wms.scanner.models.interfaces.IMainController;
import com.wms.scanner.views.adapters.DeliverRegisteredDBAdapter;
import com.wms.scanner.views.adapters.ShipOutRegisteredDBAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class DeliveryActivity extends AppCompatActivity implements IMainController {

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.back_btn)
    ImageView backBtn;
    @BindView(R.id.save_btn)
    Button saveBtn;
    @BindView(R.id.scroll_view)
    ScrollView scrollView;
    @BindView(R.id.remarks_input)
    EditText remarkInput;
    @BindView(R.id.address1_input)
    EditText address1Input;
    @BindView(R.id.address2_input)
    EditText address2Input;
    @BindView(R.id.address3_input)
    EditText address3Input;
    @BindView(R.id.address4_input)
    EditText address4Input;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.customer_input)
    EditText customerInput;

    private ApplicationController appController;
    private SessionManager sessionManager;
    private static final String TAG = DeliveryActivity.class.getSimpleName();
    private HttpController httpController;
    private Utility utility;
    private DeliverRegisteredDBAdapter adapter;
    private DatabaseHandler dbHandler;
    private ArrayList<DeliverDetailDBModel> deliverDetailDBModels;

    DeliveryHeaderModel deliveryHeaderModel;

    String soNumber = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery);
        ButterKnife.bind(this);

        appController = (ApplicationController) getApplication();
        sessionManager = new SessionManager(this);
        httpController = new HttpController(appController, this, sessionManager, TAG);
        dbHandler = new DatabaseHandler();
        utility = new Utility(this);

        soNumber = getIntent().getStringExtra("so");

        title.setText("DELIVERY");

        scrollViewSetup();

        httpController.deliverySOHeader(soNumber);

        utility.loadingCircular();

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               utility.loadingCircular();
               httpController.deliveryAddRecord(deliveryHeaderModel.getCarton(), remarkInput.getText().toString(), soNumber);
            }
        });
    }

    private void initView() {

        address1Input.setText(deliveryHeaderModel.getAddress1());
        address2Input.setText(deliveryHeaderModel.getAddress2());
        address3Input.setText(deliveryHeaderModel.getAddress3());
        address4Input.setText(deliveryHeaderModel.getAddress4());
        customerInput.setText(deliveryHeaderModel.getCustomername());

    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    protected void onResume() {
        super.onResume();
        try {
            deliverDetailDBModels = new ArrayList<>(dbHandler.getDeliverRegistered(0));
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            adapter = new DeliverRegisteredDBAdapter(this, deliverDetailDBModels);
            recyclerView.setAdapter(adapter);

            adapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void scrollViewSetup() {
        scrollView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager) appController.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                return false;
            }
        });
    }

    @Override
    public void loginDelegate(boolean success, String message, UserItems userItems) {

    }

    @Override
    public void SODetailDelegate(boolean success, ArrayList<ShipOutDetailModel> shipOutDetailModels) {

    }

    @Override
    public void addShipOutRecordDelegate(boolean success, int ID, String message) {

    }

    @Override
    public void addShipOutItemDelegate(boolean success, String message) {

    }

    @Override
    public void deliverHeaderDelegate(boolean success, DeliveryHeaderModel item) {
        utility.dismissLoadingCircular();

        if(success){
            deliveryHeaderModel = item;
            initView();

        }else{
            try {
                Toasty.error(DeliveryActivity.this, "Failed to retrieve delivery header!").show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void deliverItemDelegate(boolean success, String message) {

        utility.dismissLoadingCircular();

        if(success){
            try {
                Toasty.success(DeliveryActivity.this, message).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Intent intent = new Intent(DeliveryActivity.this, SuccessActivity.class);
            startActivity(intent);
            finish();
        }else{
            try {
                Toasty.error(DeliveryActivity.this, message).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void showMessagesDelegate(GsonItems items) {

    }
}