package com.wms.scanner.views.fragments;

import android.graphics.Color;
import android.os.Bundle;

import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toolbar;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.wms.scanner.R;
import com.wms.scanner.cores.common.DatabaseHandler;
import com.wms.scanner.cores.common.SessionManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFragment extends Fragment {

    @BindView(R.id.profile_img)
    CircleImageView profileImage;
    @BindView(R.id.name_text)
    TextView nameText;
    @BindView(R.id.designation_text)
    TextView designationText;
    @BindView(R.id.email_text)
    TextView emailText;
    @BindView(R.id.phone_text)
    TextView phoneText;
    @BindView(R.id.menu_logout)
    LinearLayout menuLogout;
    @BindView(R.id.back_btn)
    ImageView backBtn;
    @BindView(R.id.toolbar2)
    Toolbar toolbar;
    @BindView(R.id.title)
            TextView title;

    SessionManager sessionManager;
    DatabaseHandler dbHandler;


    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(ProfileFragment.this.getContext());
        dbHandler = new DatabaseHandler();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, v);

        backBtn.setVisibility(View.GONE);
        toolbar.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.colorPrimaryDark, null));
        title.setText("PROFILE");
        title.setTextColor(Color.WHITE);
        nameText.setText(sessionManager.getUserDetails().getFullname());
        emailText.setText(sessionManager.getUserDetails().getEmail());
        designationText.setText(sessionManager.getUserDetails().getDesignation());
        phoneText.setText(sessionManager.getUserDetails().getMobilephone());

        menuLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CFAlertDialog.Builder builder = new CFAlertDialog.Builder(ProfileFragment.this.getContext())
                        .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                        .setTitle("WMS")
                        .setMessage("Are you sure want to logout?")
                        .setHeaderView(R.layout.dialog_logout_header)
                        .setCancelable(true)
                        .addButton("YES", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, (dialog, which) -> {
                            dbHandler.clearDatabase();
                            sessionManager.logoutUser();
                            sessionManager.restartApp();
                            dialog.dismiss();
                        }).addButton("NO", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, (dialog, which) -> {
                                    dialog.dismiss();
                                }
                        );

                builder.show();


            }
        });

        return v;
    }
}