package com.wms.scanner.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wms.scanner.R;
import com.wms.scanner.models.data.dbdeliver.DeliverDetailDBModel;
import com.wms.scanner.models.data.dbshipout.ShipoutDetailDBModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DeliverRegisteredDBAdapter extends RecyclerView.Adapter<DeliverRegisteredDBAdapter.DeliverRegisteredViewHolder> {

    Context context;
    ArrayList<DeliverDetailDBModel> deliverDetailDBModels;

    public DeliverRegisteredDBAdapter(Context context, ArrayList<DeliverDetailDBModel> deliverDetailDBModels) {
        this.context = context;
        this.deliverDetailDBModels = deliverDetailDBModels;
    }

    @NonNull
    @Override
    public DeliverRegisteredViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lv_registered_so, parent, false);
        return new DeliverRegisteredViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull DeliverRegisteredViewHolder holder, int position) {

        DeliverDetailDBModel item = deliverDetailDBModels.get(position);

        holder.soNumberText.setText(item.getSo());

        holder.detailRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        DeliverDBQuantityAdapter adapter = new DeliverDBQuantityAdapter(item.getQuantityDetail(), context);
        holder.detailRecyclerView.setAdapter(adapter);
        float totalQuantity = 0;
        for(int x = 0; x < item.getQuantityDetail().size(); x++){
            totalQuantity = totalQuantity + Float.parseFloat(item.getQuantityDetail().get(x).getQuantity());
        }
        holder.detailText.setText("Carton: " + item.getQuantityDetail().size() + "  |  Quantity: " + totalQuantity);

    }

    @Override
    public int getItemCount() {
        return deliverDetailDBModels != null ? deliverDetailDBModels.size() : 0;
    }

    static class DeliverRegisteredViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.so_number_text)
        TextView soNumberText;
        @BindView(R.id.detail_recycler_view)
        RecyclerView detailRecyclerView;
        @BindView(R.id.detail_text)
        TextView detailText;

        public DeliverRegisteredViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
