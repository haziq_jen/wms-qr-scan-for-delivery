package com.wms.scanner.views.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wms.scanner.R;
import com.wms.scanner.controllers.ApplicationController;
import com.wms.scanner.controllers.HttpController;
import com.wms.scanner.cores.common.DatabaseHandler;
import com.wms.scanner.cores.common.ServiceSettings;
import com.wms.scanner.cores.common.SessionManager;
import com.wms.scanner.cores.common.utility.Utility;
import com.wms.scanner.models.data.GsonItems;
import com.wms.scanner.models.data.auth.UserItems;
import com.wms.scanner.models.data.dbdeliver.DeliverDetailDBModel;
import com.wms.scanner.models.data.dbdeliver.DeliverDetailQuantityModel;
import com.wms.scanner.models.data.dbshipout.ShipOutDetailQuantityModel;
import com.wms.scanner.models.data.dbshipout.ShipoutDetailDBModel;
import com.wms.scanner.models.data.deliver.DeliveryHeaderModel;
import com.wms.scanner.models.data.shipout.ShipOutDetailModel;
import com.wms.scanner.models.interfaces.IMainController;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmList;

public class CompleteScanActivity extends AppCompatActivity implements IMainController {

    @BindView(R.id.carton1_card)
    RelativeLayout carton1Card;
    @BindView(R.id.carton2_card)
    RelativeLayout carton2Card;
    @BindView(R.id.carton3_card)
    RelativeLayout carton3Card;
    @BindView(R.id.carton4_card)
    RelativeLayout carton4Card;
    @BindView(R.id.carton1_text)
    TextView carton1Text;
    @BindView(R.id.carton2_text)
    TextView carton2Text;
    @BindView(R.id.carton3_text)
    TextView carton3Text;
    @BindView(R.id.carton4_text)
    TextView carton4Text;
    @BindView(R.id.quantity1_text)
    TextView quantity1Text;
    @BindView(R.id.quantity2_text)
    TextView quantity2Text;
    @BindView(R.id.quantity3_text)
    TextView quantity3Text;
    @BindView(R.id.quantity4_text)
    TextView quantity4Text;
    @BindView(R.id.tick1)
    ImageView status1Image;
    @BindView(R.id.tick2)
    ImageView status2Image;
    @BindView(R.id.tick3)
    ImageView status3Image;
    @BindView(R.id.tick4)
    ImageView status4Image;
    @BindView(R.id.scan_btn)
    Button scanBtn;
    @BindView(R.id.complete_btn)
    Button completeBtn;
    @BindView(R.id.title)
    TextView title;

    ApplicationController appController;
    SessionManager sessionManager;
    private static final String TAG = CompleteScanActivity.class.getSimpleName();
    HttpController httpController;
    Utility utility;

    String sONumber = "";
    String cartonNumber = "";

    boolean isCarton1Scanned = false;
    boolean isCarton2Scanned = false;
    boolean isCarton3Scanned = false;
    boolean isCarton4Scanned = false;

    ArrayList<ShipOutDetailModel> shipOutDetailModels;
    RealmList<ShipOutDetailQuantityModel> shipOutDetailQuantityModels;
    RealmList<DeliverDetailQuantityModel> deliverDetailQuantityModels;

    int isAllChecked = 0;

    private DatabaseHandler dbHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_scan);
        ButterKnife.bind(this);
        appController = (ApplicationController) getApplication();
        sessionManager = new SessionManager(this);
        httpController = new HttpController(appController, this, sessionManager, TAG);
        utility = new Utility(this);
        dbHandler = new DatabaseHandler();
        sONumber = getIntent().getStringExtra("so");
        title.setText(sONumber);

        if(sessionManager.getModeView() == ServiceSettings.ADD_SHIPOUT_HEADER){
            httpController.shipoutSODetail(sONumber);
        }else if(sessionManager.getModeView() == ServiceSettings.ADD_DELIVERY_HEADER){
            httpController.deliverySODetail(sONumber);
        }

        utility.loadingCircular();

        scanBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sessionManager.getModeView() == ServiceSettings.ADD_SHIPOUT_HEADER){
                    sessionManager.setModeView(ServiceSettings.SCAN_CARTON_VIEW);
                }else if(sessionManager.getModeView() == ServiceSettings.ADD_DELIVERY_HEADER){
                    sessionManager.setModeView(ServiceSettings.SCAN_CARTON_DELIVER_VIEW);
                }

                Intent scanIntent = new Intent(CompleteScanActivity.this, QRScannerActivity.class);
                startActivityForResult(scanIntent, ServiceSettings.SCAN_CARTON_VIEW);
            }
        });

        completeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(sessionManager.getModeView() == ServiceSettings.SCAN_CARTON_VIEW){
                    shipOutDetailQuantityModels = new RealmList<>();
                    for(int x = 0; x < shipOutDetailModels.size() ; x ++){
                        ShipOutDetailQuantityModel item = new ShipOutDetailQuantityModel();
                        item.setQuantity(shipOutDetailModels.get(x).getQuantity());
                        item.setCartonNo(shipOutDetailModels.get(x).getSalesorderno()+
                                " |" + shipOutDetailModels.get(x).getCartonno() + "|");

                        shipOutDetailQuantityModels.add(item);
                    }

                    ShipoutDetailDBModel shipoutDetailDBModel = new ShipoutDetailDBModel();
                    shipoutDetailDBModel.setSo(shipOutDetailModels.get(0).getSalesorderno());
                    shipoutDetailDBModel.setStatus(true);
                    shipoutDetailDBModel.setQuantityDetail(shipOutDetailQuantityModels);

                    dbHandler.saveShipOutDetailDb(shipoutDetailDBModel);

                    Intent shipOutIntent = new Intent(CompleteScanActivity.this, ShipOutActivity.class);
                    startActivity(shipOutIntent);
                    finish();
                }else if(sessionManager.getModeView() == ServiceSettings.SCAN_CARTON_DELIVER_VIEW){

                    deliverDetailQuantityModels = new RealmList<>();
                    for(int x = 0; x < shipOutDetailModels.size() ; x ++){
                        DeliverDetailQuantityModel item = new DeliverDetailQuantityModel();
                        item.setQuantity(shipOutDetailModels.get(x).getQuantity());
                        item.setCartonNo(shipOutDetailModels.get(x).getSalesorderno()+
                                " |" + shipOutDetailModels.get(x).getCartonno() + "|");

                        deliverDetailQuantityModels.add(item);
                    }

                    DeliverDetailDBModel shipoutDetailDBModel = new DeliverDetailDBModel();
                    shipoutDetailDBModel.setSo(shipOutDetailModels.get(0).getSalesorderno());
                    shipoutDetailDBModel.setQuantityDetail(deliverDetailQuantityModels);

                    dbHandler.saveDeliverDetailDb(shipoutDetailDBModel);

                    Intent deliverIntent = new Intent(CompleteScanActivity.this, DeliveryActivity.class);
                    deliverIntent.putExtra("so", sONumber);
                    startActivity(deliverIntent);
                    finish();
                }


            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == ServiceSettings.SCAN_CARTON_VIEW){
            if(resultCode == RESULT_OK){
                assert data != null;
                cartonNumber = data.getStringExtra("carton");

                if(shipOutDetailModels.size() > 0){
                    if(cartonNumber.replaceAll("\\s+","").contains((shipOutDetailModels.get(0).getSalesorderno()+
                            " |" + shipOutDetailModels.get(0).getCartonno() + "|").replaceAll("\\s+",""))){
                        if(!isCarton1Scanned){
                            isAllChecked++;
                        }


                        isCarton1Scanned = true;

                        status1Image.setImageResource(R.drawable.ic_baseline_check_circle_24);
                    }
                }

                if(shipOutDetailModels.size() > 1){

                    if(cartonNumber.replaceAll("\\s+","").contains((shipOutDetailModels.get(1).getSalesorderno()+
                            " |" + shipOutDetailModels.get(1).getCartonno() + "|").replaceAll("\\s+",""))){
                        if(!isCarton2Scanned){
                            isAllChecked++;
                        }
                        isCarton2Scanned = true;
                        status2Image.setImageResource(R.drawable.ic_baseline_check_circle_24);
                    }
                }

                if(shipOutDetailModels.size() > 2){
                    if(cartonNumber.replaceAll("\\s+","").contains((shipOutDetailModels.get(2).getSalesorderno()+
                            " |" + shipOutDetailModels.get(2).getCartonno() + "|").replaceAll("\\s+",""))){
                        if(!isCarton3Scanned){
                            isAllChecked++;
                        }
                        isCarton3Scanned = true;
                        status3Image.setImageResource(R.drawable.ic_baseline_check_circle_24);
                    }
                }

                if(shipOutDetailModels.size() > 3){
                    if(cartonNumber.replaceAll("\\s+","").contains((shipOutDetailModels.get(3).getSalesorderno()+
                            " |" + shipOutDetailModels.get(3).getCartonno() + "|").replaceAll("\\s+",""))){
                        if(!isCarton4Scanned){
                            isAllChecked++;
                        }
                        isCarton4Scanned = true;
                        status4Image.setImageResource(R.drawable.ic_baseline_check_circle_24);
                    }
                }

                scanBtn.setText("SCAN CARTON (" + isAllChecked + "/4)");

                if(isAllChecked == shipOutDetailModels.size()){
                    scanBtn.setVisibility(View.GONE);
                    completeBtn.setVisibility(View.VISIBLE);
                }


            }
        }
    }

    private void initView(){
        carton1Card.setVisibility(shipOutDetailModels.size() > 0 ? View.VISIBLE : View.GONE);
        carton2Card.setVisibility(shipOutDetailModels.size() > 1 ? View.VISIBLE : View.GONE);
        carton3Card.setVisibility(shipOutDetailModels.size() > 2 ? View.VISIBLE : View.GONE);
        carton4Card.setVisibility(shipOutDetailModels.size() > 3 ? View.VISIBLE : View.GONE);

        carton1Text.setText(shipOutDetailModels.size() > 0 ?
                (shipOutDetailModels.get(0).getSalesorderno()+
                        " |" + shipOutDetailModels.get(0).getCartonno() + "|")
                : "");

        quantity1Text.setText(shipOutDetailModels.size()>0 ? shipOutDetailModels.get(0).getQuantity() : "");

        carton2Text.setText(shipOutDetailModels.size() > 1 ?
                (shipOutDetailModels.get(1).getSalesorderno()+
                        " |" + shipOutDetailModels.get(1).getCartonno() + "|")
                : "");

        quantity2Text.setText(shipOutDetailModels.size()>1 ? shipOutDetailModels.get(1).getQuantity() : "");

        carton3Text.setText(shipOutDetailModels.size() > 2 ?
                (shipOutDetailModels.get(2).getSalesorderno()+
                        " |" + shipOutDetailModels.get(2).getCartonno() + "|")
                : "");

        quantity3Text.setText(shipOutDetailModels.size()>2 ? shipOutDetailModels.get(2).getQuantity() : "");

        carton4Text.setText(shipOutDetailModels.size() > 3 ?
                (shipOutDetailModels.get(3).getSalesorderno()+
                        " |" + shipOutDetailModels.get(3).getCartonno() + "|")
                : "");

        quantity4Text.setText(shipOutDetailModels.size()>3 ? shipOutDetailModels.get(3).getQuantity() : "");
    }

    @Override
    public void loginDelegate(boolean success, String message, UserItems userItems) {

    }

    @Override
    public void SODetailDelegate(boolean success, ArrayList<ShipOutDetailModel> shipOutDetailModels) {

        utility.dismissLoadingCircular();

        if (success) {

            this.shipOutDetailModels = new ArrayList<>(shipOutDetailModels);
            initView();
        }

    }

    @Override
    public void addShipOutRecordDelegate(boolean success, int ID, String message) {

    }

    @Override
    public void addShipOutItemDelegate(boolean success, String message) {

    }

    @Override
    public void deliverHeaderDelegate(boolean success, DeliveryHeaderModel item) {

    }

    @Override
    public void deliverItemDelegate(boolean success, String message) {

    }

    @Override
    public void showMessagesDelegate(GsonItems items) {

    }
}