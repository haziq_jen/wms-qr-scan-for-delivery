package com.wms.scanner.views.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.wms.scanner.R;
import com.wms.scanner.models.data.dbshipout.ShipOutLocalDBModel;
import com.wms.scanner.models.interfaces.IShipOutOnClick;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShipOutHeaderDBAdapter extends RecyclerView.Adapter<ShipOutHeaderDBAdapter.ShipOutHeaderViewHolder> {

    private Context context;
    private ArrayList<ShipOutLocalDBModel> items;
    private IShipOutOnClick delegate;

    public ShipOutHeaderDBAdapter(Context context, ArrayList<ShipOutLocalDBModel> items, IShipOutOnClick delegate) {
        this.context = context;
        this.items = items;
        this.delegate = delegate;
    }

    @NonNull
    @Override
    public ShipOutHeaderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lv_ship_out_header, parent, false);
       return new ShipOutHeaderViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ShipOutHeaderViewHolder holder, @SuppressLint("RecyclerView") int position) {

        ShipOutLocalDBModel item = items.get(position);

        holder.driverText.setText("Total carton : " + item.getTotalcarton());
        holder.lorryNoText.setText(item.getLorryno());
        holder.soCountText.setText(""+item.getSoCounter());
        holder.timestampText.setText("Created at " + item.getTimestamp());
        holder.mainCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delegate.onClick(position, item.getId());
            }
        });


    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

     static class ShipOutHeaderViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.driver_text)
        TextView driverText;
        @BindView(R.id.lorry_no_text)
        TextView lorryNoText;
        @BindView(R.id.so_count_text)
        TextView soCountText;
        @BindView(R.id.timestamp)
        TextView timestampText;
        @BindView(R.id.main_card)
         CardView mainCard;

        public ShipOutHeaderViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
