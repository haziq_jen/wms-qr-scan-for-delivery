package com.wms.scanner.views.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.wms.scanner.R;
import com.wms.scanner.cores.common.ServiceSettings;
import com.wms.scanner.cores.common.SessionManager;
import com.wms.scanner.views.activities.DeliveryActivity;
import com.wms.scanner.views.activities.ShipOutActivity;
import com.wms.scanner.views.activities.ShipoutHeaderActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFragment extends Fragment {

    @BindView(R.id.ship_out_btn)
    Button shipOutBtn;
    @BindView(R.id.delivery_btn)
    Button deliveryBtn;
    @BindView(R.id.title_header)
    TextView titleHeader;

    private SessionManager sessionManager;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, v);

        sessionManager = new SessionManager(this.getContext());
        titleHeader.setText("Good Morning, " + sessionManager.getUserDetails().getFullname());

        shipOutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shipout = new Intent(HomeFragment.this.getContext(), ShipoutHeaderActivity.class);
                startActivity(shipout);
            }
        });

        deliveryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionManager.setModeView(ServiceSettings.ADD_DELIVERY_HEADER);
                Intent delivery = new Intent(HomeFragment.this.getContext(), DeliveryActivity.class);
                startActivity(delivery);
            }
        });

        return v;
    }
}