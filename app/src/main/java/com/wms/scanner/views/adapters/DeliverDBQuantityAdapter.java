package com.wms.scanner.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.wms.scanner.R;
import com.wms.scanner.models.data.dbdeliver.DeliverDetailQuantityModel;
import com.wms.scanner.models.data.dbshipout.ShipOutDetailQuantityModel;

import butterknife.BindView;
import io.realm.RealmList;

public class DeliverDBQuantityAdapter extends RecyclerView.Adapter<DeliverDBQuantityAdapter.DeliverDBQuantityViewHolder> {

    RealmList<DeliverDetailQuantityModel> items;
    Context context;

    public DeliverDBQuantityAdapter(RealmList<DeliverDetailQuantityModel> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @NonNull
    @Override
    public DeliverDBQuantityViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lv_registered_so_detail, parent, false);
        return new DeliverDBQuantityViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull DeliverDBQuantityViewHolder holder, int position) {

        DeliverDetailQuantityModel item = items.get(position);

        holder.cartonText.setText(item.getCartonNo());
        holder.quantityText.setText(item.getQuantity());

    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    static class DeliverDBQuantityViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.carton_text)
        TextView cartonText;
        @BindView(R.id.quantity_text)
        TextView quantityText;

        public DeliverDBQuantityViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
