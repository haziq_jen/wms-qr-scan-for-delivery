package com.wms.scanner.views.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.google.gson.Gson;
import com.google.zxing.Result;
import com.wms.scanner.R;
import com.wms.scanner.controllers.ApplicationController;
import com.wms.scanner.controllers.HttpController;
import com.wms.scanner.cores.common.ServiceSettings;
import com.wms.scanner.cores.common.SessionManager;
import com.wms.scanner.models.data.GsonItems;
import com.wms.scanner.models.data.auth.UserItems;
import com.wms.scanner.models.data.deliver.DeliveryHeaderModel;
import com.wms.scanner.models.data.shipout.ShipOutDetailModel;
import com.wms.scanner.models.interfaces.IMainController;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class QRScannerActivity extends AppCompatActivity implements IMainController {

    @BindView(R.id.scanner_view)
    CodeScannerView scannerView;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.back_btn)
    ImageView backBtn;

    CodeScanner mCodeScanner;
    SessionManager sessionManager;
    ApplicationController appController;
    private static final String TAG = QRScannerActivity.class.getSimpleName();
    HttpController httpController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrscanner);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        appController = (ApplicationController) getApplication();
        httpController = new HttpController(appController, this, sessionManager, TAG);

        title.setText("QR CODE SCANNER");

        if(sessionManager.getModeView() == ServiceSettings.ADD_SHIPOUT_HEADER){
            title.setText("SCAN SALES ORDER NO.");
        }else if(sessionManager.getModeView() == ServiceSettings.SCAN_CARTON_VIEW){
            title.setText("SCAN CARTON NUMBER");
        }

        mCodeScanner = new CodeScanner(this, scannerView);
        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                            System.out.println("xxxx QR Result: " + new Gson().toJson(result.getText()));

                            if(result.getText().contains("SO")){
                                if(sessionManager.getModeView() == ServiceSettings.ADD_SHIPOUT_HEADER){
                                    Intent intent = new Intent(QRScannerActivity.this, CompleteScanActivity.class);
                                    intent.putExtra("so", result.getText());
                                    startActivity(intent);
                                    finish();
                                }else if(sessionManager.getModeView() == ServiceSettings.SCAN_CARTON_VIEW){
                                    Intent intent = new Intent();
                                    intent.putExtra("carton", result.getText());
                                    setResult(RESULT_OK, intent);
                                    finish();
                                }


                            }else{
                                Toasty.error(QRScannerActivity.this, "Invalid QR code!").show();
                            }



                    }
                });
            }
        });
        scannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCodeScanner.startPreview();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        mCodeScanner.startPreview();
    }

    @Override
    protected void onPause() {
        mCodeScanner.releaseResources();
        super.onPause();
    }

    @Override
    public void loginDelegate(boolean success, String message, UserItems userItems) {

    }

    @Override
    public void SODetailDelegate(boolean success, ArrayList<ShipOutDetailModel> shipOutDetailModels) {

    }

    @Override
    public void addShipOutRecordDelegate(boolean success, int ID, String message) {

    }

    @Override
    public void addShipOutItemDelegate(boolean success, String message) {

    }

    @Override
    public void deliverHeaderDelegate(boolean success, DeliveryHeaderModel item) {

    }

    @Override
    public void deliverItemDelegate(boolean success, String message) {

    }

    @Override
    public void showMessagesDelegate(GsonItems items) {

    }
}