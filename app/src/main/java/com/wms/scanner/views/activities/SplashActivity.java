package com.wms.scanner.views.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;

import com.wms.scanner.BuildConfig;
import com.wms.scanner.MainActivity;
import com.wms.scanner.R;
import com.wms.scanner.cores.common.SessionManager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class SplashActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {

    private static final int SPLASH_TIME = 2000;
    private static final int RC_PERMISSIONS = 124;
    final static String versionName = BuildConfig.VERSION_NAME;

    private SessionManager session;

//    @BindView(R.id.version)
//    TextView version;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        session = new SessionManager(this);

//        version.setText("v" + versionName);

        getPermission();
    }


    @AfterPermissionGranted(RC_PERMISSIONS)
    public void getPermission() {

        String[] perms = {
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_PHONE_STATE
        };

        if (EasyPermissions.hasPermissions(this, perms)) {
            // Have permissions, do the thing!
            goNext();
        } else {
            // Ask for both permissions
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_location_contacts),
                    RC_PERMISSIONS, perms);
        }

        //goNext();
    }

    private void goNext() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Class<?> nextActivity;


                if(session.isLoggedIn()){
                    nextActivity = MainActivity.class;
                }else{
                    nextActivity = LoginActivity.class;
                }




                Intent i = new Intent(SplashActivity.this, nextActivity);

                startActivity(i);
                finish();

            }
        }, SPLASH_TIME);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {


    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {


        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }

    }
}