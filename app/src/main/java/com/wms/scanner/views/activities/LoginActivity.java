package com.wms.scanner.views.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.wms.scanner.MainActivity;
import com.wms.scanner.R;
import com.wms.scanner.controllers.ApplicationController;
import com.wms.scanner.controllers.HttpController;
import com.wms.scanner.cores.common.SessionManager;
import com.wms.scanner.cores.common.utility.Utility;
import com.wms.scanner.models.data.GsonItems;
import com.wms.scanner.models.data.auth.UserItems;
import com.wms.scanner.models.data.deliver.DeliveryHeaderModel;
import com.wms.scanner.models.data.shipout.ShipOutDetailModel;
import com.wms.scanner.models.interfaces.IMainController;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class LoginActivity extends AppCompatActivity implements IMainController {
    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.sign_in_btn)
    Button signInBtn;
    @BindView(R.id.email_input)
    EditText emailInput;
    @BindView(R.id.password_input)
    EditText passwordInput;
    @BindView(R.id.remember_me_checkbox)
    CheckBox rememberMeCheckBox;

    private ApplicationController appController;
    private SessionManager sessionManager;
    private final static String TAG = LoginActivity.class.getSimpleName();
    private HttpController httpController;
    private Utility utility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        appController = (ApplicationController) getApplication();
        sessionManager = new SessionManager(this);
        httpController = new HttpController(appController, this, sessionManager, TAG);
        utility = new Utility(this);

        rememberMeCheckBox.setChecked(sessionManager.getRememberEmail() != null && sessionManager.getRememberPassword() != null);

        emailInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(rememberMeCheckBox.isChecked()){
                    sessionManager.setRememberedEmail(s.toString());
                }else{
                    sessionManager.setRememberedEmail(null);
                }
            }
        });

        passwordInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(rememberMeCheckBox.isChecked()){
                    sessionManager.setRememberedPassword(s.toString());
                }else{
                    sessionManager.setRememberedPassword(null);
                }
            }
        });

        rememberMeCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    sessionManager.setRememberedEmail(emailInput.getText().toString());
                    sessionManager.setRememberedPassword(passwordInput.getText().toString());
                }else{
                    sessionManager.setRememberedEmail(null);
                    sessionManager.setRememberedPassword(null);
                }
            }
        });

        signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(emailInput.getText().toString().isEmpty() || passwordInput.getText().toString().isEmpty()){
                    utility.alertError("Login Error!", "Oops, please complete the form!");
                }else{
                    httpController.login(emailInput.getText().toString(), passwordInput.getText().toString());
                    utility.loadingCircular();
                }

            }
        });
    }

    @Override
    public void showMessagesDelegate(GsonItems items) {

    }

    @Override
    public void loginDelegate(boolean success, String message, UserItems userItems) {

        utility.dismissLoadingCircular();

        if(success){
            sessionManager.createLoginSession(userItems, true);
            try {
                Toasty.success(LoginActivity.this, message).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
            sessionManager.restartApp();
        }else{
            try {
                Toasty.error(LoginActivity.this, message).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void SODetailDelegate(boolean success, ArrayList<ShipOutDetailModel> shipOutDetailModels) {

    }

    @Override
    public void addShipOutRecordDelegate(boolean success, int ID, String message) {

    }

    @Override
    public void addShipOutItemDelegate(boolean success, String message) {

    }

    @Override
    public void deliverHeaderDelegate(boolean success, DeliveryHeaderModel item) {

    }

    @Override
    public void deliverItemDelegate(boolean success, String message) {

    }
}