package com.wms.scanner.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wms.scanner.R;
import com.wms.scanner.models.data.dbshipout.ShipOutDetailQuantityModel;
import com.wms.scanner.models.data.dbshipout.ShipoutDetailDBModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShipOutRegisteredDBAdapter extends RecyclerView.Adapter<ShipOutRegisteredDBAdapter.ShipOutRegisteredViewHolder> {

    Context context;
    ArrayList<ShipoutDetailDBModel> shipOutDetailItems;

    public ShipOutRegisteredDBAdapter(Context context, ArrayList<ShipoutDetailDBModel> shipOutDetailItems) {
        this.context = context;
        this.shipOutDetailItems = shipOutDetailItems;
    }

    @NonNull
    @Override
    public ShipOutRegisteredViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lv_registered_so, parent, false);
       return new ShipOutRegisteredViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ShipOutRegisteredViewHolder holder, int position) {

        ShipoutDetailDBModel item = shipOutDetailItems.get(position);

        holder.soNumberText.setText(item.getSo());

        holder.detailRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        ShipOutDBQuantityAdapter adapter = new ShipOutDBQuantityAdapter(item.getQuantityDetail(), context);
        holder.detailRecyclerView.setAdapter(adapter);
        float totalQuantity = 0;
        for(int x = 0; x < item.getQuantityDetail().size(); x++){
            totalQuantity = totalQuantity + Float.parseFloat(item.getQuantityDetail().get(x).getQuantity());
        }
        holder.detailText.setText("Carton: " + item.getQuantityDetail().size() + "  |  Quantity: " + totalQuantity);

    }

    @Override
    public int getItemCount() {
        return shipOutDetailItems != null ? shipOutDetailItems.size() : 0;
    }

    static class ShipOutRegisteredViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.so_number_text)
        TextView soNumberText;
        @BindView(R.id.detail_recycler_view)
        RecyclerView detailRecyclerView;
        @BindView(R.id.detail_text)
        TextView detailText;

        public ShipOutRegisteredViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
