package com.wms.scanner.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.wms.scanner.R;
import com.wms.scanner.models.data.dbshipout.ShipOutDetailQuantityModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmList;
import io.realm.RealmResults;

public class ShipOutDBQuantityAdapter extends RecyclerView.Adapter<ShipOutDBQuantityAdapter.ShipOutQuantityViewHolder> {

  RealmList<ShipOutDetailQuantityModel> items;
    Context context;

    public ShipOutDBQuantityAdapter(RealmList<ShipOutDetailQuantityModel> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @NonNull
    @Override
    public ShipOutQuantityViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lv_registered_so_detail, parent, false);
        return new ShipOutQuantityViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ShipOutQuantityViewHolder holder, int position) {

        ShipOutDetailQuantityModel item = items.get(position);

        holder.cartonText.setText(item.getCartonNo());
        holder.quantityText.setText(item.getQuantity());

    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    static class ShipOutQuantityViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.carton_text)
        TextView cartonText;
        @BindView(R.id.quantity_text)
        TextView quantityText;

        public ShipOutQuantityViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
