package com.wms.scanner.views.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.wms.scanner.R;
import com.wms.scanner.cores.common.DatabaseHandler;
import com.wms.scanner.cores.common.ServiceSettings;
import com.wms.scanner.cores.common.SessionManager;
import com.wms.scanner.cores.common.ValidationManager;
import com.wms.scanner.models.data.dbshipout.ShipOutLocalDBModel;
import com.wms.scanner.models.interfaces.IShipOutOnClick;
import com.wms.scanner.views.adapters.ShipOutHeaderDBAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShipoutHeaderActivity extends AppCompatActivity implements IShipOutOnClick {

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.no_data_layout)
    LinearLayout noDataLayout;
    @BindView(R.id.add_btn)
    FloatingActionButton addBtn;
    @BindView(R.id.back_btn)
    ImageView bacVkBtn;

    ShipOutHeaderDBAdapter adapter;
    DatabaseHandler dbHandler;

    SessionManager session;

    private ArrayList<ShipOutLocalDBModel> items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipout_header);
        ButterKnife.bind(this);
        dbHandler = new DatabaseHandler();
        session = new SessionManager(this);
        title.setText("SHIP OUT RECORD");



        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.setModeView(ServiceSettings.ADD_SHIPOUT_HEADER);
                Intent addIntent = new Intent(ShipoutHeaderActivity.this, QRScannerActivity.class);
                startActivity(addIntent);
            }
        });

        bacVkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    @SuppressLint("NotifyDataSetChanged")
    @Override
    protected void onResume() {
        super.onResume();

        try {
            items = new ArrayList<>(dbHandler.getShipOutHeaderDb());

            adapter = new ShipOutHeaderDBAdapter(ShipoutHeaderActivity.this, items, ShipoutHeaderActivity.this );
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(adapter);

            adapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(dbHandler.getShipOutHeaderDb().size() == 0){
            noDataLayout.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }else{
            noDataLayout.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(int position, int ID) {

        Intent intent = new Intent(ShipoutHeaderActivity.this, ShipOutActivity.class);
        intent.putExtra("id", ID);
        startActivity(intent);
    }
}