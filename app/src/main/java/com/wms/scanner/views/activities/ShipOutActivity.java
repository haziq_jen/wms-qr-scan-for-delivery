package com.wms.scanner.views.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.wms.scanner.R;
import com.wms.scanner.controllers.ApplicationController;
import com.wms.scanner.controllers.HttpController;
import com.wms.scanner.cores.common.DatabaseHandler;
import com.wms.scanner.cores.common.ServiceSettings;
import com.wms.scanner.cores.common.SessionManager;
import com.wms.scanner.cores.common.ValidationManager;
import com.wms.scanner.cores.common.utility.Utility;
import com.wms.scanner.models.data.GsonItems;
import com.wms.scanner.models.data.auth.UserItems;
import com.wms.scanner.models.data.dbshipout.ShipOutLocalDBModel;
import com.wms.scanner.models.data.dbshipout.ShipoutDetailDBModel;
import com.wms.scanner.models.data.deliver.DeliveryHeaderModel;
import com.wms.scanner.models.data.shipout.ShipOutDetailModel;
import com.wms.scanner.models.interfaces.IMainController;
import com.wms.scanner.views.adapters.ShipOutRegisteredDBAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import io.realm.RealmList;

public class ShipOutActivity extends AppCompatActivity implements IMainController {

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.back_btn)
    ImageView backBtn;
    @BindView(R.id.add_btn)
    FloatingActionButton scanBtn;
    @BindView(R.id.save_btn)
    Button saveBtn;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.lorry_no_input)
    EditText lorryNoInput;
    @BindView(R.id.remarks_input)
    EditText remarksInput;
    @BindView(R.id.so_input)
    EditText totalSoInput;
    @BindView(R.id.carton_input)
    EditText totalCartonInput;
    @BindView(R.id.scroll_view)
    ScrollView scrollView;

    private ApplicationController appController;
    private SessionManager sessionManager;
    private HttpController httpController;
    private static final String TAG = ShipOutActivity.class.getSimpleName();
    private ShipOutRegisteredDBAdapter adapter;
    private DatabaseHandler dbHandler;
    private boolean isUpdateView = false;
    private int ID = 0;
    private ShipOutLocalDBModel shipOutLocalDBModel;
    private ArrayList<ShipoutDetailDBModel> shipOutDetailModel;
    private Utility utility;
    int totalCarton = 0;
    int sendDataCounter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ship_out);
        ButterKnife.bind(this);
        appController = (ApplicationController) getApplication();
        sessionManager = new SessionManager(this);
        httpController = new HttpController(appController, this, sessionManager, TAG);
        dbHandler = new DatabaseHandler();
        utility = new Utility(this);
        title.setText("ADD SHIP OUT RECORD");

        ID = getIntent().getIntExtra("id", 0);


        if (getIntent().getIntExtra("id", 0) != 0) {
            isUpdateView = true;
            ID = getIntent().getIntExtra("id", 0);
            shipOutLocalDBModel = dbHandler.getShipOutLocalByID(ID);
            updateView();
        }

        initView();

        scanBtn.setVisibility(isUpdateView ? View.GONE : View.VISIBLE);
        saveBtn.setVisibility(isUpdateView ? View.GONE : View.VISIBLE);

    }


    private void initView() {

//        System.out.println("xxxx shipoutdetail " + new Gson().toJson(dbHandler.getShipOutRegistered(ID)));
        scrollViewSetup();


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        scanBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionManager.setModeView(ServiceSettings.ADD_SHIPOUT_HEADER);
                Intent qr = new Intent(ShipOutActivity.this, QRScannerActivity.class);
                startActivity(qr);
            }
        });

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                utility.loadingCircular();
                httpController.shipoutAddRecord(totalCartonInput.getText().toString(),
                        "",
                        lorryNoInput.getText().toString(),
                        "",
                        remarksInput.getText().toString());
            }
        });


    }

    @SuppressLint("ClickableViewAccessibility")
    private void scrollViewSetup() {
        scrollView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager) appController.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                return false;
            }
        });
    }


    private void updateView() {

        lorryNoInput.setText(shipOutLocalDBModel.getLorryno());
        lorryNoInput.setEnabled(false);
        totalSoInput.setEnabled(false);

        totalCartonInput.setEnabled(false);

        remarksInput.setText(shipOutLocalDBModel.getRemarks());
        remarksInput.setEnabled(false);

    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    protected void onResume() {
        super.onResume();

        try {
            shipOutDetailModel = new ArrayList<>(dbHandler.getShipOutRegistered(ID));
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            adapter = new ShipOutRegisteredDBAdapter(this, shipOutDetailModel);
            recyclerView.setAdapter(adapter);

            adapter.notifyDataSetChanged();

            totalSoInput.setText("" + shipOutDetailModel.size());
            totalSoInput.setEnabled(false);

            for (int x = 0; x < shipOutDetailModel.size(); x++) {
                totalCarton = totalCarton + shipOutDetailModel.get(x).getQuantityDetail().size();
            }

            totalCartonInput.setText("" + totalCarton);
            totalCartonInput.setEnabled(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void loginDelegate(boolean success, String message, UserItems userItems) {

    }

    @Override
    public void SODetailDelegate(boolean success, ArrayList<ShipOutDetailModel> shipOutDetailModels) {

    }

    @Override
    public void addShipOutRecordDelegate(boolean success, int ID, String message) {

        if (success) {
            dbHandler.updateIdInsideRegisteredSO(ID);

            ShipOutLocalDBModel shipOutLocalDBModel = new ShipOutLocalDBModel();
            shipOutLocalDBModel.setTotalcarton(String.valueOf(totalCarton));
            shipOutLocalDBModel.setRemarks(remarksInput.getText().toString());
            shipOutLocalDBModel.setTimestamp(ValidationManager.getTimeStamp());
            shipOutLocalDBModel.setLorryno(lorryNoInput.getText().toString());
            shipOutLocalDBModel.setId(ID);
            shipOutLocalDBModel.setTotalquantity("");
            shipOutLocalDBModel.setSoCounter(shipOutDetailModel.size());
            RealmList<ShipoutDetailDBModel> items = new RealmList<>();
            items.addAll(shipOutDetailModel);
            shipOutLocalDBModel.setDetailList(items);
            shipOutLocalDBModel.setDriver("");

            dbHandler.saveShipOutHeaderToDb(shipOutLocalDBModel);

            for (int x = 0; x < shipOutDetailModel.size(); x++) {
                httpController.shipoutAddItemRecord(String.valueOf(ID), shipOutDetailModel.get(x).getSo());
            }


            try {
                Toasty.success(ShipOutActivity.this, message).show();
            } catch (Exception e) {
                e.printStackTrace();
            }


        } else {
            try {
                utility.dismissLoadingCircular();
                Toasty.error(ShipOutActivity.this, message).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void addShipOutItemDelegate(boolean success, String message) {

        if (success) {

            sendDataCounter++;

            if (sendDataCounter == shipOutDetailModel.size()) {

                utility.dismissLoadingCircular();

                try {
                    Toasty.success(ShipOutActivity.this, message).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(ShipOutActivity.this, SuccessActivity.class);
                startActivity(intent);
                finish();

            }

        } else {
            try {
                utility.dismissLoadingCircular();
                Toasty.error(ShipOutActivity.this, message).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void deliverHeaderDelegate(boolean success, DeliveryHeaderModel item) {

    }

    @Override
    public void deliverItemDelegate(boolean success, String message) {

    }

    @Override
    public void showMessagesDelegate(GsonItems items) {

    }
}