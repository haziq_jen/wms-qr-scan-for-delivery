package com.wms.scanner.models.data;

import com.wms.scanner.BuildConfig;

public enum DomainItems {

    LIVE("LIVE","Live", BuildConfig.LIVE_URL),
    DEMO("DEMO","Demo",BuildConfig.DEMO_URL);

    private String domainKey;
    private String displayName;
    private String domainURL;

    DomainItems(String domainKey, String domainName, String domainURL){
        this.domainKey = domainKey;
        this.displayName = domainName;
        this.domainURL = domainURL;
    }

    public String getDomainKey() {
        return domainKey;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getDomainURL() {
        return domainURL;
    }

    public static String getDefaultUrl(){
        return DomainItems.LIVE.domainURL;
    }

    public static String getDefaultKey(){
        return DomainItems.LIVE.domainKey;
    }

    static public boolean isMember(String aName) {
       DomainItems[] aFruits = DomainItems.values();
        for (DomainItems aFruit : aFruits)
            if (aFruit.domainKey.equals(aName))
                return true;
        return false;
    }
}
