package com.wms.scanner.models.interfaces;

import com.wms.scanner.models.data.auth.UserItems;
import com.wms.scanner.models.data.deliver.DeliveryHeaderModel;
import com.wms.scanner.models.data.shipout.ShipOutDetailModel;

import java.util.ArrayList;

public interface IMainController extends IMessageModel{

    void loginDelegate(boolean success, String message, UserItems userItems);
    void SODetailDelegate(boolean success, ArrayList<ShipOutDetailModel> shipOutDetailModels);
    void addShipOutRecordDelegate(boolean success, int ID, String message);
    void addShipOutItemDelegate(boolean success, String message);
    void deliverHeaderDelegate(boolean success, DeliveryHeaderModel item);
    void deliverItemDelegate(boolean success, String message);
}
