package com.wms.scanner.models.interfaces;


import com.wms.scanner.models.data.GsonItems;

public interface IMessageModel {
    void showMessagesDelegate(GsonItems items);
}
