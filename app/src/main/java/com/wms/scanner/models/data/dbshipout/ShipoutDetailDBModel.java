package com.wms.scanner.models.data.dbshipout;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ShipoutDetailDBModel extends RealmObject {

    @PrimaryKey
    String so;

    boolean status;
    int id;
    RealmList<ShipOutDetailQuantityModel> quantityDetail;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSo() {
        return so;
    }

    public void setSo(String so) {
        this.so = so;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public RealmList<ShipOutDetailQuantityModel> getQuantityDetail() {
        return quantityDetail;
    }

    public void setQuantityDetail(RealmList<ShipOutDetailQuantityModel> quantityDetail) {
        this.quantityDetail = quantityDetail;
    }
}
