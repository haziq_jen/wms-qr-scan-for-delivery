package com.wms.scanner.models.data.auth;

public class UserModel {

    boolean status;
    String msg;
    UserItems data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public UserItems getData() {
        return data;
    }

    public void setData(UserItems data) {
        this.data = data;
    }
}
