package com.wms.scanner.models.interfaces;

import com.android.volley.VolleyError;
import com.wms.scanner.models.data.GsonItems;

public interface IGsonRequestModel {

    public void volleySuccessDelegate(GsonItems items);
    public void volleyErrorDelegate(GsonItems items, VolleyError error);

}
