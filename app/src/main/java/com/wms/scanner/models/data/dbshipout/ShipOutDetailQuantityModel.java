package com.wms.scanner.models.data.dbshipout;

import io.realm.RealmObject;

public class ShipOutDetailQuantityModel extends RealmObject {

    String cartonNo;
    String quantity;

    public String getCartonNo() {
        return cartonNo;
    }

    public void setCartonNo(String cartonNo) {
        this.cartonNo = cartonNo;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
