package com.wms.scanner.models.data.dbshipout;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ShipOutLocalDBModel extends RealmObject {

    @PrimaryKey
    int id;

    String totalcarton;
    String totalquantity;
    String lorryno;
    String driver;
    String remarks;
    String timestamp;

    int soCounter;

    RealmList<ShipoutDetailDBModel> detailList;

    public RealmList<ShipoutDetailDBModel> getDetailList() {
        return detailList;
    }

    public void setDetailList(RealmList<ShipoutDetailDBModel> detailList) {
        this.detailList = detailList;
    }

    public int getSoCounter() {
        return soCounter;
    }

    public void setSoCounter(int soCounter) {
        this.soCounter = soCounter;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTotalcarton() {
        return totalcarton;
    }

    public void setTotalcarton(String totalcarton) {
        this.totalcarton = totalcarton;
    }

    public String getTotalquantity() {
        return totalquantity;
    }

    public void setTotalquantity(String totalquantity) {
        this.totalquantity = totalquantity;
    }

    public String getLorryno() {
        return lorryno;
    }

    public void setLorryno(String lorryno) {
        this.lorryno = lorryno;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
