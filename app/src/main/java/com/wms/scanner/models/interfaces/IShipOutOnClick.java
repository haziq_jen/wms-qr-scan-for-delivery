package com.wms.scanner.models.interfaces;

public interface IShipOutOnClick {

    void onClick(int position, int ID);
}
