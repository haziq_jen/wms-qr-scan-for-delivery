package com.wms.scanner.models.data.dbdeliver;

import com.wms.scanner.models.data.dbshipout.ShipOutDetailQuantityModel;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class DeliverDetailDBModel extends RealmObject {

    @PrimaryKey
    String so;
    int id;
    RealmList<DeliverDetailQuantityModel> quantityDetail;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSo() {
        return so;
    }

    public void setSo(String so) {
        this.so = so;
    }

    public RealmList<DeliverDetailQuantityModel> getQuantityDetail() {
        return quantityDetail;
    }

    public void setQuantityDetail(RealmList<DeliverDetailQuantityModel> quantityDetail) {
        this.quantityDetail = quantityDetail;
    }
}
