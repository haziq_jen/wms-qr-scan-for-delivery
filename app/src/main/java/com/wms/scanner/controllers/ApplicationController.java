package com.wms.scanner.controllers;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ContentResolver;
import android.content.Context;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

import androidx.appcompat.app.AppCompatDelegate;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.wms.scanner.cores.common.SessionManager;
import com.wms.scanner.models.data.DomainItems;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class ApplicationController extends Application {

    public static final String TAG = ApplicationController.class
            .getSimpleName();
    private static ApplicationController mInstance;
    private RequestQueue mRequestQueue;

//    private UserItems userProfile;
    private SessionManager sessionManager;
    public static final String CHANNEL_1_ID = "channel1";
    public static final String CHANNEL_2_ID = "channel2";


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        getSessionManager();
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name("wms_db.realm")
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
        createNotificationChannels();
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public void cancelPendingRequests() {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(TAG);
        }
    }


    public static synchronized ApplicationController getInstance() {
        return mInstance;
    }
//
//    public UserItems getUserProfile() {
//        return userProfile;
//    }
//
//    public void setUserProfile(UserItems userProfile) {
//        this.userProfile = userProfile;
//    }

    public SessionManager getSessionManager() {
        if (sessionManager == null) {
            sessionManager = new SessionManager(this);
        }

        return sessionManager;
    }

//
//    public UserItems getUserDetails(){
//        return this.sessionManager.getUserDetails();
//    }
//
    public String getDomainName(){

        String domain = sessionManager.getKeyDomainUrl();
        //System.out.println("domain = " + domain);

        if(DomainItems.isMember(domain)){
            //System.out.println("url = " + DomainItems.valueOf(sessionManager.getKeyDomainUrl()).getDomainURL());
            return DomainItems.valueOf(sessionManager.getKeyDomainUrl()).getDomainURL();
        }
        else{
            //System.out.println("url = " + DomainItems.LIVE.getDomainURL());
            return DomainItems.getDefaultUrl();
        }
    }

    public int getSleepBrightness(){
        return 4;
    }

    public boolean screenBrightness(boolean isAuto) {
        return screenBrightness(0,isAuto);
    }
    public boolean screenBrightness(int level) {
        Log.i(TAG, "xx screenBrightness: "+level);
        return screenBrightness(level,false);
    }

    public boolean screenBrightness(int level, boolean isAuto) {

        try {
//            android.provider.Settings.System.putInt(
//                    this.getContentResolver(),
//                    android.provider.Settings.System.SCREEN_BRIGHTNESS, level);

            Settings.System.putInt(this.getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS_MODE,
                    isAuto? Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC :
                            Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);

            if(!isAuto){
                Settings.System.putInt(
                        this.getContentResolver(),
                        Settings.System.SCREEN_BRIGHTNESS,
                        level);
            }

            return true;
        }

        catch (Exception e) {
            Log.e("Screen Brightness", "error changing screen brightness");
            return false;
        }
    }

    // Get the screen current brightness
    public int getScreenBrightness(){
        /*
            public static int getInt (ContentResolver cr, String name, int def)
                Convenience function for retrieving a single system settings value as an integer.
                Note that internally setting values are always stored as strings; this function
                converts the string to an integer for you. The default value will be returned
                if the setting is not defined or not an integer.

            Parameters
                cr : The ContentResolver to access.
                name : The name of the setting to retrieve.
                def : Value to return if the setting is not defined.
            Returns
                The setting's current value, or 'def' if it is not defined or not a valid integer.
        */
        int brightnessValue = Settings.System.getInt(
                this.getContentResolver(),
                Settings.System.SCREEN_BRIGHTNESS,
                0
        );
        return brightnessValue;
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        //MultiDex.install(this);
    }


    private void createNotificationChannels() {

//        Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + this.getPackageName() + "/" + R.raw.hudhud_noti); //Here is FILE_NAME is the name of file that you want to play
//        AudioAttributes audioAttributes = new AudioAttributes.Builder()
//                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
//                .setUsage(AudioAttributes.USAGE_ALARM)
//                .build();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {


            NotificationChannel channel1 = new NotificationChannel(
                    CHANNEL_1_ID,
                    "Channel 1",
                    NotificationManager.IMPORTANCE_HIGH
            );
            channel1.setDescription("This is Channel 1");
//            channel1.setSound(sound, audioAttributes);

            NotificationChannel channel2 = new NotificationChannel(
                    CHANNEL_2_ID,
                    "Channel 2",
                    NotificationManager.IMPORTANCE_LOW
            );
            channel2.setDescription("This is Channel 2");
//            channel2.setSound(sound, audioAttributes);
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel1);
            manager.createNotificationChannel(channel2);
        }
    }
}
