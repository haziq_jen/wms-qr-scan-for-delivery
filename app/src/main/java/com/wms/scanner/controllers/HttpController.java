package com.wms.scanner.controllers;

import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.wms.scanner.cores.common.DatabaseHandler;
import com.wms.scanner.cores.common.ServiceSettings;
import com.wms.scanner.cores.common.SessionManager;
import com.wms.scanner.cores.common.utility.JSONParser;
import com.wms.scanner.cores.http.GsonRequestV2;
import com.wms.scanner.models.data.GsonItems;
import com.wms.scanner.models.data.auth.UserItems;
import com.wms.scanner.models.data.deliver.DeliveryHeaderModel;
import com.wms.scanner.models.data.shipout.AddShipOutRecordModel;
import com.wms.scanner.models.data.shipout.ShipOutDetailModel;
import com.wms.scanner.models.interfaces.IGsonRequestModel;
import com.wms.scanner.models.interfaces.IMainController;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public class HttpController implements IGsonRequestModel {

    //Login
    private final static int LOGIN = 1001;

    //Ship Out
    private final static int SHIPOUT_SO_HEADER = 1002;
    private final static int SHIPOUT_SO_DETAIL = 1003;
    private final static int SHIPOUT_ADD_RECORD = 1004;
    private final static int SHIPOUT_ADD_ITEM_RECORD = 1005;

    //Delivery
    private final static int DELIVERY_SO_HEADER = 1006;
    private final static int DELIVERY_SO_DETAIL = 1007;
    private final static int DELIVERY_ADD_RECORD = 1008;
    private final static int DELIVERY_ADD_ITEM_RECORD = 1008;


    private ApplicationController appController;
    private IMainController delegate;
    private SessionManager session;
    private String TAG;
    private DatabaseHandler dbHandler;
    private JSONParser JParser = new JSONParser();

    public HttpController(ApplicationController appController, IMainController delegate, SessionManager session, String TAG) {
        this.appController = appController;
        this.delegate = delegate;
        this.session = session;
        this.TAG = TAG;
        this.dbHandler = new DatabaseHandler();
    }

    public void login(String username, String password) {

        HashMap<String, String> params = new HashMap<String, String>();

        params.put("username", username);
        params.put("password", password);

        String endPoint = ServiceSettings.LOGIN.replace(ServiceSettings.SERVER_ROOT_URL,
                appController.getDomainName());

        GsonRequestV2 jsObjRequest = new GsonRequestV2(
                appController,
                Request.Method.POST,
                endPoint,
                params,
                null,
                LOGIN,
                this
        );

        appController.getInstance().addToRequestQueue(jsObjRequest, TAG);
    }

    public void shipoutSOHeader(String SO) {

        HashMap<String, String> params = new HashMap<String, String>();

        String endPoint = ServiceSettings.SHIPOUT_SO_HEADER.replace(ServiceSettings.SERVER_ROOT_URL,
                appController.getDomainName()) + SO;

        GsonRequestV2 jsObjRequest = new GsonRequestV2(
                appController,
                Request.Method.GET,
                endPoint,
                params,
                null,
                SHIPOUT_SO_HEADER,
                this
        );

        appController.getInstance().addToRequestQueue(jsObjRequest, TAG);
    }

    public void shipoutSODetail(String SO) {

        HashMap<String, String> params = new HashMap<String, String>();

        String endPoint = ServiceSettings.SHIPOUT_SO_DETAIL.replace(ServiceSettings.SERVER_ROOT_URL,
                appController.getDomainName()) + SO;

        GsonRequestV2 jsObjRequest = new GsonRequestV2(
                appController,
                Request.Method.GET,
                endPoint,
                params,
                null,
                SHIPOUT_SO_DETAIL,
                this
        );

        appController.getInstance().addToRequestQueue(jsObjRequest, TAG);
    }

    public void shipoutAddRecord( String totalCarton, String totalQuantity, String lorryNo,
                                 String driver, String remark) {

        HashMap<String, String> params = new HashMap<String, String>();

        params.put("userid", session.getUserDetails().getId());
        params.put("totalcarton", totalCarton);
//        params.put("totalquantity", totalQuantity);
        params.put("lorryno", lorryNo);
//        params.put("driver", driver);
        params.put("remarks", remark);
        params.put("username", session.getUserDetails().getUsername());

        String endPoint = ServiceSettings.SHIPOUT_ADD_RECORD.replace(ServiceSettings.SERVER_ROOT_URL,
                appController.getDomainName());

        GsonRequestV2 jsObjRequest = new GsonRequestV2(
                appController,
                Request.Method.POST,
                endPoint,
                params,
                null,
                SHIPOUT_ADD_RECORD,
                this
        );

        appController.getInstance().addToRequestQueue(jsObjRequest, TAG);
    }

    public void shipoutAddItemRecord(String id, String SO) {

        HashMap<String, String> params = new HashMap<String, String>();

        params.put("id", id);
        params.put("salesorderno", SO);

        String endPoint = ServiceSettings.SHIPOUT_ADD_ITEM_RECORD.replace(ServiceSettings.SERVER_ROOT_URL,
                appController.getDomainName());

        GsonRequestV2 jsObjRequest = new GsonRequestV2(
                appController,
                Request.Method.POST,
                endPoint,
                params,
                null,
                SHIPOUT_ADD_ITEM_RECORD,
                this
        );

        appController.getInstance().addToRequestQueue(jsObjRequest, TAG);
    }

    public void deliveryAddItemRecord(String id, String SO) {

        HashMap<String, String> params = new HashMap<String, String>();

        params.put("id", id);
        params.put("salesorderno", SO);

        String endPoint = ServiceSettings.DELIVERY_ADD_ITEM_RECORD.replace(ServiceSettings.SERVER_ROOT_URL,
                appController.getDomainName());

        GsonRequestV2 jsObjRequest = new GsonRequestV2(
                appController,
                Request.Method.POST,
                endPoint,
                params,
                null,
                DELIVERY_ADD_ITEM_RECORD,
                this
        );

        appController.getInstance().addToRequestQueue(jsObjRequest, TAG);
    }


    public void deliveryAddRecord(String totalCarton, String remark, String so) {

        HashMap<String, String> params = new HashMap<String, String>();

        params.put("userid", session.getUserDetails().getId());
        params.put("totalcarton", totalCarton);
        params.put("remarks", remark);
        params.put("salesorderno", so);
        params.put("username", session.getUserDetails().getUsername());

        String endPoint = ServiceSettings.DELIVERY_ADD_RECORD.replace(ServiceSettings.SERVER_ROOT_URL,
                appController.getDomainName());

        GsonRequestV2 jsObjRequest = new GsonRequestV2(
                appController,
                Request.Method.POST,
                endPoint,
                params,
                null,
                DELIVERY_ADD_RECORD,
                this
        );

        appController.getInstance().addToRequestQueue(jsObjRequest, TAG);
    }

    public void deliverySODetail(String SO) {

        HashMap<String, String> params = new HashMap<String, String>();

        String endPoint = ServiceSettings.DELIVERY_SO_DETAIL.replace(ServiceSettings.SERVER_ROOT_URL,
                appController.getDomainName()) + SO;

        GsonRequestV2 jsObjRequest = new GsonRequestV2(
                appController,
                Request.Method.GET,
                endPoint,
                params,
                null,
                DELIVERY_SO_DETAIL,
                this
        );

        appController.getInstance().addToRequestQueue(jsObjRequest, TAG);
    }

    public void deliverySOHeader(String SO) {

        HashMap<String, String> params = new HashMap<String, String>();

        String endPoint = ServiceSettings.DELIVERY_SO_HEADER.replace(ServiceSettings.SERVER_ROOT_URL,
                appController.getDomainName()) + SO;

        GsonRequestV2 jsObjRequest = new GsonRequestV2(
                appController,
                Request.Method.GET,
                endPoint,
                params,
                null,
                DELIVERY_SO_HEADER,
                this
        );

        appController.getInstance().addToRequestQueue(jsObjRequest, TAG);
    }


    @Override
    public void volleySuccessDelegate(GsonItems items) {
        boolean status = JParser.getJsonStatus(items.getJsonString(),
                ServiceSettings.TAG_STATUS
        );

        String response = JParser.getJsonData(items.getJsonString(), ServiceSettings.TAG_MESSAGE);

        System.out.println("xxxxx MAINCONTROLLER " + items.getReqCode() + " " + items.getJsonString());

        if (items.getReqCode() == LOGIN) {

            UserItems model = new Gson().fromJson(items.getJsonString(), UserItems.class);

            delegate.loginDelegate(true, "Login Success", model);


        }

        if(items.getReqCode() == SHIPOUT_SO_DETAIL){

            JSONArray model = JParser.JSONArray(items.getJsonString());

            try{
                ArrayList<ShipOutDetailModel> shipOutDetailModels = new ArrayList<>();

                for(int x = 0 ; x < model.length(); x++){
                    ShipOutDetailModel data = new Gson().fromJson(model.getString(x), ShipOutDetailModel.class);
                    shipOutDetailModels.add(data);
                }

                delegate.SODetailDelegate(true, shipOutDetailModels);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if(items.getReqCode() == SHIPOUT_ADD_RECORD){
            AddShipOutRecordModel model = new Gson().fromJson(items.getJsonString(), AddShipOutRecordModel.class);
            delegate.addShipOutRecordDelegate(true, model.getID(), model.getMessage());
        }

        if(items.getReqCode() == SHIPOUT_ADD_ITEM_RECORD){
            delegate.addShipOutItemDelegate(true, response);
        }

        if(items.getReqCode() == DELIVERY_SO_HEADER){
            DeliveryHeaderModel model = new Gson().fromJson(items.getJsonString(), DeliveryHeaderModel.class);
            delegate.deliverHeaderDelegate(true, model);
        }


    }

    @Override
    public void volleyErrorDelegate(GsonItems items, VolleyError error) {

        System.out.println("xxxxx MAINCONTROLLER " + items.getReqCode() + " " + items.getJsonString()
                + "  error : " + error.getMessage());

        String response = JParser.getJsonData(items.getJsonString(), ServiceSettings.TAG_MESSAGE);

        if (items.getJsonString().equals(ServiceSettings.TAG_TOKEN_EXPIRED) ||
                items.getJsonString().equals(ServiceSettings.TAG_TOKEN_BLACKLISTED)) {
            session.logoutUser();
            session.restartApp();

            Toast.makeText(appController.getApplicationContext(),
                    items.getJsonString(),
                    Toast.LENGTH_LONG).show();
        } else {
            delegate.showMessagesDelegate(items);
        }

        if(items.getReqCode() == LOGIN){
            delegate.loginDelegate(false, response, null);
        }

        if(items.getReqCode() == SHIPOUT_SO_DETAIL){
            delegate.SODetailDelegate(false, null);
        }

        if(items.getReqCode() == SHIPOUT_ADD_RECORD){
            delegate.addShipOutRecordDelegate(false, 0, response);
        }

        if(items.getReqCode() == SHIPOUT_ADD_ITEM_RECORD){
            delegate.addShipOutItemDelegate(false, response);
        }

        if(items.getReqCode() == DELIVERY_SO_HEADER){
            delegate.deliverHeaderDelegate(false, null);
        }
    }
}
