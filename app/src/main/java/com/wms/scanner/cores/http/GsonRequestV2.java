package com.wms.scanner.cores.http;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.wms.scanner.controllers.ApplicationController;
import com.wms.scanner.cores.common.ServiceSettings;
import com.wms.scanner.cores.common.SessionManager;
import com.wms.scanner.cores.common.utility.JSONParser;
import com.wms.scanner.models.data.GsonItems;
import com.wms.scanner.models.interfaces.IGsonRequestModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by HaziqJen.
 */
public class GsonRequestV2 extends Request<GsonItems> {

    private ApplicationController applicationController;
    //private final Response.Listener mListener;
    private IGsonRequestModel delegate;

    private HashMap<String, String> arrayBody;
    private String mBody;
    private String mContentType;
    private HashMap<String, String> header = null;
    private String url;
    private int ReqCode = 0;

    public GsonRequestV2(int method,
                         String url,
                         String body,
                         HashMap<String, String> headers,
                         int ReqCode,
                         IGsonRequestModel delegate) {

        super(method, url,null);

        this.mBody = body;
        this.header = headers;
        //this.mListener = registerSuccessListener();
        this.delegate = delegate;
        this.url = url;
        this.ReqCode = ReqCode;
        this.mContentType = "application/json";

        RetryPolicy policy = new DefaultRetryPolicy(60 * 1000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        );
        setRetryPolicy(policy);

    }

    public GsonRequestV2(ApplicationController applicationController,
                         int method,
                         String url,
                         HashMap<String, String> arrayBody,
                         HashMap<String, String> headers,
                         int ReqCode,
                         IGsonRequestModel delegate) {

        super(method, url,null);

        this.applicationController = applicationController;
        this.arrayBody = arrayBody;
        this.header = headers;
        this.delegate = delegate;
        this.url = url;
        this.ReqCode = ReqCode;
        this.mContentType = "application/json";

        RetryPolicy policy = new DefaultRetryPolicy(60 * 1000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        );
        setRetryPolicy(policy);
    }


    @Override
    protected Response<GsonItems> parseNetworkResponse(NetworkResponse response) {

        try {

            //String sessionId = response.headers.get("Set-Cookie");
            //System.out.println("ci_session : " + sessionId);

            String jsonString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));


            GsonItems items = getGsonItems(jsonString, response.statusCode);

            return Response.success(items,
                    HttpHeaderParser.parseCacheHeaders(response));


        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        }

    }

    @Override
    protected VolleyError parseNetworkError(VolleyError volleyError) {
        return super.parseNetworkError(volleyError);
    }

    //On Volley Success
    @Override
    protected void deliverResponse(GsonItems items) {
        System.out.println("xxxxxxxx deliverResponse" + items.getJsonString());

        String[] json = isValidJSON(items.getJsonString());
        System.out.println("xxxxx response " + new Gson().toJson(items));
        if(Boolean.parseBoolean(json[0])){
            delegate.volleySuccessDelegate(items);
        }else {
            String jsonErr = "{ \"status\":\"false\" , \"msg\":\""+json[1]+"\" }";
            items.setJsonString(jsonErr);
            delegate.volleySuccessDelegate(items);
        }
    }

    //On Volley Error
    @Override
    public void deliverError(VolleyError volleyError) {
        //super.deliverError(error);
        JSONParser JParser = new JSONParser();
        int statusCode = 0;
        String errorMessage = null;

        System.out.println("xxxxxxxx deliverError " + volleyError.getMessage());
        if (volleyError instanceof NoConnectionError) {

            errorMessage = "{\"statusCode\":100, \"message\":\"No internet Access, Check your internet connection\"}";

            delegate.volleyErrorDelegate(getGsonItems(errorMessage, statusCode), volleyError);
            return;
        }else if(volleyError instanceof TimeoutError){
//
            errorMessage = "{\"statusCode\":100, \"message\":\"Connection Timeout. Please try again\"}";
            delegate.volleyErrorDelegate(getGsonItems(errorMessage, statusCode),volleyError);
        }
        else if(volleyError instanceof NetworkError){
//
            errorMessage = "{\"statusCode\":100, \"message\":\"Network error. Please try again\"}";
            delegate.volleyErrorDelegate(getGsonItems(errorMessage, statusCode),volleyError);
        }
        else if(volleyError instanceof ParseError){
//
            errorMessage = "{\"statusCode\":100, \"message\":\"Parse error. Please try again\"}";
            delegate.volleyErrorDelegate(getGsonItems(errorMessage, statusCode),volleyError);
        }
        else{
            try {
                errorMessage = new String(volleyError.networkResponse.data, "utf-8");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            String[] json = isValidJSON(errorMessage);
            if (Boolean.parseBoolean(json[0])) {

                delegate.volleyErrorDelegate(getGsonItems(errorMessage, statusCode), volleyError);

            } else {
                String jsonErr = "{ \"statusCode\": 500 , \"message\":\"" + "Invalid Data" + "\" }";
                delegate.volleyErrorDelegate(getGsonItems(jsonErr, statusCode), volleyError);
            }
        }

    }

    private GsonItems getGsonItems(String jsonString, int statusCode){

        jsonString = (jsonString == null)? ServiceSettings.TAG_INTERNAL_SERVER_ERROR: jsonString;
        Log.d("xx failed",jsonString);

        //get key auth_token in row body n save to Session Manager
//        if(jsonString.contains(ServiceSettings.TAG_AUTH_TOKEN)){
//
//            String auth_token = new JSONParser().getJsonData(jsonString,
//                    ServiceSettings.TAG_AUTH_TOKEN
//            );
//
//            if(auth_token.length() > 2){
//                ApplicationController.getInstance().getSessionManager().setUserSession(
//                        auth_token
//                );
//            }
//        }

        GsonItems items = new GsonItems();
        items.setStatusCode(statusCode);
        items.setJsonString(jsonString);
        items.setUrl(this.url);
        items.setReqCode(this.ReqCode);

        return items;
    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {

        HashMap<String, String> headers;
        if(this.header != null){
            headers = this.header;
        }
        else{
            SessionManager sm = applicationController.getSessionManager();
            headers = new HashMap<String, String>();
            headers.put("Authorization", "Bearer " + (sm.getTokenId() == null ? "" : sm.getTokenId()));
        }

        return headers;
    }


    @Override
    public byte[] getBody() throws AuthFailureError {

        if(arrayBody != null){
            SessionManager sm = applicationController.getSessionManager();

            return new JSONObject(arrayBody).toString().getBytes();


        }
        return mBody.getBytes();
    }

    @Override
    public String getBodyContentType() {
        return mContentType;
    }

    public String getContentType() {
        return mContentType;
    }

    public void setContentType(String mContentType) {
        this.mContentType = mContentType;
    }

    public String[] isValidJSON(String json) {
        try {
            new JSONObject(json);
        } catch (JSONException ex) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(json);

            } catch (JSONException ex1) {
                if(json.length() == 0){
                    return new String[] {
                            String.valueOf(false),
                            "Server return empty Json ::"
                    };
                }
                else{
                    return new String[] {String.valueOf(false),"Invalid Json :: "+json};
                }

            }
        }
        return new String[] {String.valueOf(true)};
    }
}
