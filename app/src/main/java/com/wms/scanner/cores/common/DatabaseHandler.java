package com.wms.scanner.cores.common;

import static com.wms.scanner.controllers.ApplicationController.TAG;

import android.util.Log;

import com.wms.scanner.models.data.dbdeliver.DeliverDetailDBModel;
import com.wms.scanner.models.data.dbshipout.ShipOutLocalDBModel;
import com.wms.scanner.models.data.dbshipout.ShipoutDetailDBModel;
import com.wms.scanner.models.data.shipout.ShipOutDetailModel;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

public class DatabaseHandler {

    Realm realm;

    public DatabaseHandler() {
        this.realm = Realm.getDefaultInstance();
    }

    public void saveShipOutHeaderToDb(ShipOutLocalDBModel items){
        try{
            realm.beginTransaction();
            realm.copyToRealm(items);
            realm.commitTransaction();
            realm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void saveShipOutDetailDb(ShipoutDetailDBModel items){
        try {
            realm.beginTransaction();
            realm.copyToRealm(items);
            realm.commitTransaction();
            realm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void saveDeliverDetailDb(DeliverDetailDBModel items){
        try {
            realm.beginTransaction();
            realm.copyToRealm(items);
            realm.commitTransaction();
            realm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public ArrayList<ShipOutLocalDBModel> getShipOutHeaderDb(){
        return new ArrayList<>(realm.where(ShipOutLocalDBModel.class).findAll());
    }

    public ShipOutLocalDBModel getShipOutLocalByID(int id){
        return realm.where(ShipOutLocalDBModel.class).equalTo("id", id).findFirst();
    }

    public ArrayList<ShipoutDetailDBModel> getShipOutRegistered(int id){
        return new ArrayList<>(realm.where(ShipoutDetailDBModel.class).equalTo("id", id).findAll());
    }

    public ArrayList<DeliverDetailDBModel> getDeliverRegistered(int id){
        return new ArrayList<>(realm.where(DeliverDetailDBModel.class).equalTo("id", id).findAll());
    }

    public void updateIdInsideRegisteredSO(int id){

        RealmResults<ShipoutDetailDBModel> items = realm.where(ShipoutDetailDBModel.class).equalTo("id", 0)
                .findAll();
        realm.beginTransaction();
        for(int x = 0; x < items.size(); x++){
            items.get(x).setId(id);
        }

        realm.commitTransaction();
    }


    public int getIdPrimaryShipOutHeaderDB(){
        Number maxId = realm.where(ShipOutLocalDBModel.class).max("id");
        return  (maxId == null) ? 1 : maxId.intValue() + 1;
    }

    public void clearDatabase(){
        try {
            realm.close();
            Realm.deleteRealm(realm.getConfiguration());
        } catch (Exception e) {
            Log.e(TAG, "removeAllData:" + e.getMessage());
        }
    }


}
