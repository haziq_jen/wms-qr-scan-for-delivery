package com.wms.scanner.cores.common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by FatahZullAppreka on 8/3/15.
 */
public class ValidationManager {

    public static final String DATE_START = "date_start";
    public static final String DATE_END = "date_end";
    public static final String TIME_START = "time_start";
    public static final String TIME_END = "time_end";

    //Mobile No validation
    public boolean isValidMobileNo(String s, int country_code)
    {
        boolean check;

        if(country_code == ServiceSettings.MALAYSIA_CODE)
        {

            if(s.substring(0, 2).equals("01") && s.length() > 9){

                check = isInteger(s);
            }
            else{
                check = false;
            }
        }
        else
        {
            if(s.length() > 9){

                check = isInteger(s);
            }
            else{
                check = false;
            }
        }

        return check;
    }

    public boolean isInteger(String s){

        boolean check;

        String NRIC_STRING = "\\d+";

        Pattern p = Pattern.compile(NRIC_STRING);
        Matcher m = p.matcher(s);
        check = m.matches();

        return check;
    }

    public String TrimMobileNo(String contact_no){

        String no = null;

        if (contact_no.contains("+6")){

            String trim = contact_no.substring(2).trim();
            String trim_no = trim.replaceAll(" ", "");

            no = String.valueOf(normalizePhoneNumber(trim_no));

            System.out.println("Graph trim +6 : " + no);
        }
        else if (contact_no.charAt(0) != "0".charAt(0)){

            String trim = contact_no.substring(1).trim();
            String trim_no = trim.replaceAll(" ", "");

            no = String.valueOf(normalizePhoneNumber(trim_no));

            System.out.println("Graph trim 6 : " + no);
        }
        else{
            no = String.valueOf(normalizePhoneNumber(contact_no));
        }

        return no;
    }


    private String normalizePhoneNumber(String phoneNumber) {

        return phoneNumber.replaceAll("[-+^]*", "");
    }

    public DateFormat getDateTime(String type){

        // Locale for formatter
        //Locale malaysianLocale = new Locale("en", "MY");

        DateFormat dateTimeFormat;

        if(type.equals(DATE_START) || type.equals(DATE_END)){

            // Default date and time format for Malaysia
            dateTimeFormat = new SimpleDateFormat("EEE, MMM d, yyyy");
        }
        else{

            // Default date and time format for Malaysia
            dateTimeFormat = new SimpleDateFormat("h:mm a");
        }


        // This step is crucial
        TimeZone malaysianTimeZone = TimeZone.getTimeZone("Asia/Kuala_Lumpur");
        dateTimeFormat.setTimeZone(malaysianTimeZone);

        return dateTimeFormat;
    }

    public String dateOut(String mytime){

        //String mytime="Jan 17, 2012";
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "EEE, MMM d, yyyy");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(mytime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd");
        String finalDate = timeFormat.format(myDate);

        return finalDate;
    }

    public String dateIn(String mytime){

        //String mytime="Jan 17, 2012";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(mytime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat timeFormat = new SimpleDateFormat("EEE, MMM d, yyyy");
        String finalDate = timeFormat.format(myDate);

        return finalDate;
    }

    public String timeIn(String mytime){

        //String mytime="Jan 17, 2012";
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "HH:mm:ss");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(mytime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a");

        String finalDate = timeFormat.format(myDate);

        return finalDate;
    }

    public String timeOut(String mytime){

        //String mytime="Jan 17, 2012";
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "h:mm a");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(mytime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        String finalDate = timeFormat.format(myDate);

        return finalDate;
    }

    public static String getTimeStamp(){

        Calendar c = Calendar.getInstance();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        // This step is crucial
        TimeZone malaysianTimeZone = TimeZone.getTimeZone("Asia/Kuala_Lumpur");
        dateformat.setTimeZone(malaysianTimeZone);

        return dateformat.format(c.getTime());
    }
    public int getDiffTime(String d1, String d2){

        try
        {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date1 = format.parse(d1);
            Date date2 = format.parse(d2);
            System.out.println("xx date1  " +date1);
            System.out.println("xx date2  " +date2);
            long mills = date1.getTime() - date2.getTime();
            int hours = (int) (mills/(1000 * 60 * 60));
            int mins = (int) ((mills/1000)/60);

//            return hours;
            return mins;

            //String diff = hours + ":" + mins; // updated value every1 second
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return 0;
    }

    public static Date getDateFormat(String timestamp){
        Date date1 = null;
        try
        {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date1 = format.parse(timestamp);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return date1;
    }

    public int getDiffMin(String d1, String d2){

        try
        {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date1 = format.parse(d1);
            Date date2 = format.parse(d2);
            System.out.println("xx date1  " +date1);
            System.out.println("xx date2  " +date2);
            long mills = date1.getTime() - date2.getTime();
            int hours = (int) (mills/(1000 * 60 * 60));
            int mins = (int) (mills/(1000*60)) % 60;

//            return hours;
            return mins;

            //String diff = hours + ":" + mins; // updated value every1 second
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return 0;
    }

    public int getPlusFourHour(String d1, String d2){

        try
        {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date1 = format.parse(d1);
            Date date2 = format.parse(d2);
            System.out.println("xx date1  " +date1);
            System.out.println("xx date2  " +date2);
            long mills = date1.getTime() + 14400000;
            int hours = (int) (mills/(1000 * 60 * 60));
            int mins = (int) (mills/(1000*60)) % 60;

            return hours;
//            return mins;

            //String diff = hours + ":" + mins; // updated value every1 second
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return 0;
    }

    public String timeStampIn(String dateStr) {

        if(dateStr == null || dateStr.length() == 0){
            dateStr = getTimeStamp();
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timestamp = "";
        //today = today.length() < 2 ? "0" + today : today;

        try {
            Date date = format.parse(dateStr);
            SimpleDateFormat todayFormat = new SimpleDateFormat("dd");
            String dateToday = todayFormat.format(date);
            format = new SimpleDateFormat("dd MMM yyyy HH:mm");
            //format = dateToday.equals(today) ? new SimpleDateFormat("hh:mm a") : new SimpleDateFormat("dd LLL, hh:mm a");
            String date1 = format.format(date);
            timestamp = date1.toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return timestamp;
    }

    public static String currentDate() {

        Calendar c = Calendar.getInstance();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");

        // This step is crucial
        TimeZone malaysianTimeZone = TimeZone.getTimeZone("Asia/Kuala_Lumpur");
        dateformat.setTimeZone(malaysianTimeZone);

        return dateformat.format(c.getTime());
    }

    public boolean isValidDate(String dateStart, String dateEnd){

        SimpleDateFormat dfDate  = new SimpleDateFormat("yyyy-MM-dd");
        boolean b = false;
        try {
            if(dfDate.parse(dateStart).before(dfDate.parse(dateEnd)))
            {
                b = true;//If start date is before end date
            }
            else if(dfDate.parse(dateStart).equals(dfDate.parse(dateEnd)))
            {
                b = true;//If two dates are equal
            }
            else
            {
                b = false; //If start date is after the end date
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return b;
    }

    public boolean isOnline(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public void dumbLogToFile(){
        try {
            String filePath = Environment.getExternalStorageDirectory() + "/logcat.txt";
            Runtime.getRuntime().exec(new String[]{"logcat", "-v", "time", "-f", filePath, "*:E"});

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


}



