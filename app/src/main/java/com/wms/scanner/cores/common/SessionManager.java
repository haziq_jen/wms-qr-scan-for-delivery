package com.wms.scanner.cores.common;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.wms.scanner.models.data.DomainItems;
import com.wms.scanner.models.data.auth.UserItems;
import com.wms.scanner.views.activities.SplashActivity;

import java.util.HashMap;
import java.util.Set;


public class SessionManager {
	// Shared Preferences
	SharedPreferences pref;
	SharedPreferences pref1;

	// Editor for Shared preferences
	Editor editor;
	Editor editorNoti;

	// Context
	Context _context;

	// Shared pref mode
	int PRIVATE_MODE = 0;
	int PRIVATE_MODE_NOTI = 0;

	// Sharedpref file name
	private static final String PREF_NAME = "SystemData";

	private static final String PREF_NOTI = "UserData";

	// All Shared Preferences Keys
	private static final String IS_LOGIN = "IsLoggedIn";
	private static final String KEY_TOKEN_ID = "token_id";
	private static final String IS_UNAUTHORIZED = "IsUnauthorized";
	public static final String REMEMBER_EMAIL = "remembered_email";
	public static final String REMEMBER_PASSWORD = "remembered_password";
	public static final String SAVE_EMAIL = "saved_email";
	public static final String SAVE_PASSWORD = "saved_password";
	public static final String KEY_MODE_VIEW = "view_mode";
	public static final String KEY_DOMAIN_URL = "domain";
	public static final String KEY_ID = "id";
	public static final String KEY_USERNAME = "username";
	public static final String KEY_FULL_NAME = "full_name";
	public static final String KEY_EMAIL = "email";
	public static final String KEY_GENDER = "gender";
	public static final String KEY_MOBILE_PHONE = "mobile_phone";
	public static final String KEY_DESIGNATION = "designation";
	public static final String KEY_PASSWORD = "password";

	// Constructor
	public SessionManager(Context context){
		this._context = context;
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		pref1 = _context.getSharedPreferences(PREF_NOTI, PRIVATE_MODE_NOTI);
		editor = pref.edit();
		editorNoti = pref1.edit();
	}

	/**
	 * Create login session
	 * */
	public void createLoginSession(UserItems userProfile, boolean isLogin){
		// Storing login value as TRUE
		editorNoti.putBoolean(IS_LOGIN, isLogin);

		// Storing id in pref
		editorNoti.putString(KEY_ID, userProfile.getId());

		editorNoti.putString(KEY_USERNAME, userProfile.getUsername());

		// Storing id in pref
		editorNoti.putString(KEY_FULL_NAME, userProfile.getFullname());

		// Storing id in pref
		editorNoti.putString(KEY_GENDER, userProfile.getGender());

		// Storing id in pref
		editorNoti.putString(KEY_MOBILE_PHONE, userProfile.getMobilephone());

		// Storing email in pref
		editorNoti.putString(KEY_EMAIL, userProfile.getEmail());

		// Storing IC in pref
		editorNoti.putString(KEY_DESIGNATION, userProfile.getDesignation());

		// Storing Image in pref
		editorNoti.putString(KEY_PASSWORD, userProfile.getPassword());

		// commit changes
		editor.commit();
		editorNoti.commit();
	}



	public void setIsUnauthorized(boolean isUnauthorized){
		editorNoti.putBoolean(IS_UNAUTHORIZED, isUnauthorized);
		editor.commit();
		editorNoti.commit();
	}

	public void setRememberedEmail(String email){
		editor.putString(REMEMBER_EMAIL, email);
		editor.commit();
		editorNoti.commit();
	}

	public String getRememberEmail(){
		return pref.getString(REMEMBER_EMAIL, null);
	}

	public String getRememberPassword(){
		return pref.getString(REMEMBER_PASSWORD, null);
	}

	public void setRememberedPassword(String password){
		editor.putString(REMEMBER_PASSWORD, password);
		editor.commit();
		editorNoti.commit();
	}

	public void saveEmail(String email){
		editor.putString(SAVE_EMAIL, email);
		editor.commit();
		editorNoti.commit();
	}

	public void savePassword(String password){
		editor.putString(SAVE_PASSWORD, password);
		editor.commit();
		editorNoti.commit();
	}

	/**
	 * Quick check for login
	 * **/
	// Get Login State
	public boolean isLoggedIn(){
		return pref1.getBoolean(IS_LOGIN, false);
	}

	public String getTokenId(){
		return pref1.getString(KEY_TOKEN_ID, null);
	}

	public void clearModeView(){
		editorNoti.putString(KEY_MODE_VIEW, null);
		editorNoti.commit();
	}


	/**
	 * Update DOmain URL
	 * */
	public void setKeyDomainUrl(String domainUrl){
		// Storing Session in pref
		editorNoti.putString(KEY_DOMAIN_URL, domainUrl);
		// commit changes
		editorNoti.commit();
	}

	/**
	 * Get Domain URL
	 * */
	public String getKeyDomainUrl(){
		// return Domain URL
		return pref1.getString(KEY_DOMAIN_URL, DomainItems.LIVE.getDomainKey());
	}



	/**
	 * Save ModeView
	 * */
	public void setModeView(int mode){

		// Storing name in pref
		editorNoti.putInt(KEY_MODE_VIEW, mode);

		// commit changes
		editorNoti.commit();
	}

	/**
	 * Get ModeView
	 * */
	public int getModeView(){
		return pref1.getInt(KEY_MODE_VIEW, 0);
	}





	public UserItems getUserDetails(){

		UserItems userProfile = new UserItems();
		userProfile.setId(pref1.getString(KEY_ID, null));
		userProfile.setUsername(pref1.getString(KEY_USERNAME, null));
		userProfile.setFullname(pref1.getString(KEY_FULL_NAME, null));
		userProfile.setGender(pref1.getString(KEY_GENDER, null));
		userProfile.setEmail(pref1.getString(KEY_EMAIL, null));
		userProfile.setMobilephone(pref1.getString(KEY_MOBILE_PHONE, null));
		userProfile.setDesignation(pref1.getString(KEY_DESIGNATION, null));
		userProfile.setPassword(pref1.getString(KEY_PASSWORD, null));

		// return user
		return userProfile;
	}



	/**
	 * Clear Session and Force Logout
	 * */
	public void restartApp(){

		// After logout redirect user to Loing Activity
		Intent i = new Intent(_context, SplashActivity.class);
		// Closing all the Activities
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

		// Add new Flag to start new Activity
		i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

		// Staring Login Activity
		_context.startActivity(i);
	}


	/**
	 * Clear Session and Force Logout
	 * */
	public void logoutUser(){
		// Clearing all data from Shared Preferences
		editorNoti.clear();
		editorNoti.commit();
	}


}
