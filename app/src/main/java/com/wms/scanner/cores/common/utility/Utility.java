package com.wms.scanner.cores.common.utility;

import android.content.Context;
import android.view.Gravity;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.wms.scanner.R;

public class Utility {

    private Context context;
    KProgressHUD kProgressHUD;

    public Utility(Context context) {
        this.context = context;
    }

    public void alertError(String title, String message) {
        // Create Alert using Builder
        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                .setDialogStyle(CFAlertDialog.CFAlertStyle.NOTIFICATION)
                .setHeaderView(R.layout.cfalert_error_header)
                .setTextGravity(Gravity.CENTER)
                .setTitle(title)
                .setMessage(message)
                .addButton("OKAY",
                        -1,
                        -1,
                        CFAlertDialog.CFAlertActionStyle.POSITIVE,
                        CFAlertDialog.CFAlertActionAlignment.CENTER,
                        (dialog, which) -> dialog.dismiss());

        builder.show();

    }

    public void loadingCircular() {
        kProgressHUD = new KProgressHUD(context);
        kProgressHUD = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please Wait")
                .setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        kProgressHUD.show();
    }

    public void dismissLoadingCircular() {
        try {
            if (kProgressHUD != null) {
                kProgressHUD.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
