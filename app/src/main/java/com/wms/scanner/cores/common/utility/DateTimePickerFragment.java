package com.wms.scanner.cores.common.utility;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatDialogFragment;


import com.wms.scanner.cores.common.ValidationManager;

import java.util.Calendar;

/**
 * Created by fatahzull on 06/08/2019.
 */

public class DateTimePickerFragment extends AppCompatDialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        if(getTag().equals(ValidationManager.DATE_START) ||
                getTag().equals(ValidationManager.DATE_END))
        {

            DatePickerDialog dialog = new DatePickerDialog(getActivity(),
                    (DatePickerDialog.OnDateSetListener) getActivity(), year, month, day);

            dialog.getDatePicker().setTag(getTag());
            //dialog.getDatePicker().setMinDate(c.getTimeInMillis());

            return dialog;
        }
        else
        {

            TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                    (TimePickerDialog.OnTimeSetListener)getActivity(), hour, minute,false);

            return timePickerDialog;
        }
    }

}
