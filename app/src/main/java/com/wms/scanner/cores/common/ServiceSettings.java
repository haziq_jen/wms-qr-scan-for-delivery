package com.wms.scanner.cores.common;

import android.app.Activity;

/**
 * Created by haziq_jen in 2020
 */

public class ServiceSettings extends Activity {

    public static final String SERVER_ROOT_URL = "_dom_";
    public static final String FCM_URL = "https://fcm.googleapis.com/fcm/send";
    public static final String STATIC_MAP_URL = "https://maps.googleapis.com/maps/api/staticmap?zoom=19&size=500x400&key=AIzaSyBu-916DdpKAjTmJNIgngS6HL_kDIKU0aU&markers=color:red|";

    /**
     * WMS APIs
     */

    public final static String LOGIN = SERVER_ROOT_URL +"user/login";

    public static final String SHIPOUT_SO_HEADER = SERVER_ROOT_URL + "shipout/salesorder/";
    public static final String SHIPOUT_SO_DETAIL = SERVER_ROOT_URL + "shipout/salesorderdet/";
    public static final String SHIPOUT_ADD_RECORD = SERVER_ROOT_URL + "shipout/add";
    public static final String SHIPOUT_ADD_ITEM_RECORD = SERVER_ROOT_URL + "shipout/additem";

    public static final String DELIVERY_SO_HEADER = SERVER_ROOT_URL + "deliver/salesorder/";
    public static final String DELIVERY_SO_DETAIL = SERVER_ROOT_URL + "deliver/salesorderdet/";
    public static final String DELIVERY_ADD_RECORD = SERVER_ROOT_URL + "deliver/add";
    public static final String DELIVERY_ADD_ITEM_RECORD = SERVER_ROOT_URL + "deliver/additem";

    public final static int MALAYSIA_CODE = 60;

    public final static String LOCALE_ENGLISH = "en";
    public final static String LOCALE_MALAY = "ms";

    // id to handle the notification in the notification try
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
    public static final int RC_PERMISSIONS = 103;
    public static final String APP_ID = "005";
    public static final int C_PERMISSIONS = 124;;

    public final static String TAG_STATUS = "status";
    public final static String TAG_STATUS_CODE = "status_code";
    public final static String TAG_STATUS_CODE_ERROR = "statusCode";
    public final static String TAG_DATA = "data";
    public final static String TAG_TOKEN_EXPIRED = "Token has expired";
    public final static String TAG_TOKEN_BLACKLISTED = "The token has been blacklisted";
    public final static String TAG_INTERNAL_SERVER_ERROR = "Internal Server Error";
    public final static String TAG_MESSAGE = "message";
    public final static String TAG_ERROR = "error";
    public final static String TAG_AUTH_TOKEN = "auth_token";
    public final static String TAG_MOBI_ID = "mobi_id";
    public final static String TAG_KEY = "key";
    public final static String TAG_TO = "to";
    public final static String TAG_USERS = "users";



    //MODE VIEW
    public final static int ADD_SHIPOUT_HEADER = 9001;
    public final static int SHIPOUT_VIEW = 9002;
    public final static int ADD_DELIVERY_HEADER = 9003;
    public static final int DELIVERY_VIEW = 9004;
    public static final int SCAN_CARTON_VIEW = 9005;
    public static final int SCAN_CARTON_DELIVER_VIEW = 9006;

    //ACTIVITY RESULT
    public final static int RESULT_EDIT_BANK = 101;
    public final static int RESULT_ADD_BANK = 102;
    public final static int RESULT_UPDATE_PROFILE = 103;
    public final static int RESULT_REQUEST_BACKGROUND_LOCATION_PERMS = 107;


    public static final int DONT_HAVE_PIN_VERIFICATION = 104;
    public static final int ALREADY_HAVE_PIN_VERIFICATION = 105;
    public static final int RESET_PIN = 106;
}
