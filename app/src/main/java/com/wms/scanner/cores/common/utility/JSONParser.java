package com.wms.scanner.cores.common.utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by FatahZullAppreka on 7/9/15.
 */
public class JSONParser {

	public String getJsonData(final String response, final String key) {

		String data = null;
		try {
			JSONObject jsonResponse = new JSONObject(response);
			data = jsonResponse.getString(key);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			data = e.getMessage();
		}

		return data;
	}

	public boolean getJsonStatus(final String response, final String key) {

		boolean success = false;
		try {
			JSONObject jsonResponse = new JSONObject(response);
			success = jsonResponse.getBoolean("status");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return success;
	}
	
	public JSONObject JSONObject(final String response) {

		JSONObject jsonResponse = null;
		try {
			jsonResponse = new JSONObject(response);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonResponse;
	}

	public JSONArray JSONArray(final String response) {

		JSONArray jsonResponse = null;
		try {
			jsonResponse = new JSONArray(response);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonResponse;
	}
	
	public JSONArray getJSONArray(final JSONObject response, final String key) {

		JSONArray jsonResponse = null;
		try {
			jsonResponse = response.getJSONArray(key);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonResponse;
	}
	
	
	public JSONObject getJSONObject(final JSONArray response, int i) {

		JSONObject jsonResponse = null;
		try {
			jsonResponse = response.getJSONObject(i);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonResponse;
	}

	public JSONObject createJSONObject(String[] key, String[] values){

		JSONObject params = new JSONObject();
		try {
			for (int i = 0; i < key.length ; i++) {
				params.put(key[i], values[i]);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return params;
	}

}
